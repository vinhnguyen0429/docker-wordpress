# Wordpress Docker Compose

#### Easy Wordpress development with Docker and Docker Compose

## Starting a new project

Make sure you have the latest versions of **Docker** and **Docker Compose** installed on your machine.
Copy the **docker-compose.yml** file from this repository into a blank folder.

In the file you may change the IP address (in case you run multiple containers) or the database from mysql to mariadb.

##### Create containers

Open a terminal and *cd* to the folder you have the docker-compose.yml and run:
```
sudo docker-compose up
```

This create 2 new folders beside your docker-compose.yml file.
* **wp-data** - used to store and restore database dumps
* **wp-app** - the location of your Wordpress application

The containers are now build and running. You should be able to access the Wordpress installation with the configured IP in the browser address. For convenience you may add a new entry into your hosts file.

##### Strating containers

You can start the containers with the up command in daemon mode (by adding **-d** as a param) or by using the start command:
```
sudo docker-compose start
```

##### Stopping containers
```
sudo docker-compose stop
```

##### Remove containers

To stop and remove all the containers use the **down** command
```
sudo docker-compose down
```
... or the **rm** command if the containers are stopped already.
```
sudo docker-compose rm --all
```

## Accessing database locally via Client
Login details
```
    WORDPRESS_DB_HOST: 127.0.0.1
    WORDPRESS_DB_NAME: wordpress
    WORDPRESS_DB_USER: root
    WORDPRESS_DB_PASSWORD: password
```      

## Developing a Theme

Open a terminal and *cd* to the folder where your themes are.
e.g below
```
/Users/username/Desktop/docker/docker-wordpress/wp-app/wp-content/themes
```

This is where you will be doing all your custom themes.

## Hostname

Add project.test (or your chosen hostname) to /etc/hosts, e.g.:
```
127.0.0.1 project.test
```


### Useful Docker Commands

| Commands                 | Description                                    |
| ------------------------ | ---------------------------------------------- |
| `docker ps`              | List Containers                                |
| `docker-compose up -d`   | Create and start containers with detached flag |
| `docker-compose stop`    | Stop running containers                        |
| `docker-compose rmi`     | Remove container images                        |
| `docker-compose restart` | Restart services                               |
| `docker kill`            | Force stop service containers                  |
| `docker-compose up -d --force-recreate` | Force recreate containers       |
| `docker stop $(docker ps -a -q)` | Stop all containers                    |
| `docker rm -f $(docker ps -a -q)` | Remove all containers                 |
| `docker rmi -f $(docker images -q)` | Delete all images                   |
| `docker system prune --all` | Remove unused images                        |
| `docker volume rm $(docker volume ls -qf dangling=true)` | Remove volumes |

### Cleanup

| Commands                 | Description                                    |
| ------------------------ | ---------------------------------------------- |
| `docker stop $(docker ps -a -q)` | Stop all containers                    |
| `docker rm -f $(docker ps -a -q)` | Remove all containers                 |
| `docker volume rm $(docker volume ls -qf dangling=true)` | Remove volumes |
| `docker rm $(docker ps --all -q -f status=exited)` | Removes all hanging  |
